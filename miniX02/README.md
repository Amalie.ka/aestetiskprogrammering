# Æstetisk programmering
### MiniX02
[Link to MiniX02 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX02?ref_type=heads)

![](miniX02.gif)

Run the code [here!](https://Amalieka.gitlab.io/aestetiskprogrammering/miniX02/minix02.html)

View the code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX02/minix02sketch.js?ref_type=heads) :)
<br>
<br>

**What have you produced, used and learnt?**

I have created who different emojis, the first being interactive in that it changes color when you hover over it and the second emoji is an angel.

The first emoji consists of a lot of rectangles, where I worked with both `strokeWeight()` and corner-radius in an 5 or 8-parameter rectangle: `rect(x, y, w, [h], [tl], [tr], [br], [bl])`. An example of this, it the first finger fromt the left with 5 parameters:

```
rect(windowWidth / 4 - 125, windowHeight / 2 - 65, 75, 150, 20);
```

Furthermore I created `if-else` statements for the color change of both the skin color (`fill()`) and the "shadow" of the fingers (`stroke()`), which was also great practice and an opportunity of improvement and learning:

```
function colorOfEmoji1() {
  if (mouseX > windowWidth / 4 - 150 && mouseX < windowWidth / 4 + 165 && mouseY > windowHeight / 2 - 117.5 && mouseY < windowHeight / 2 + 250) {
    fill(130, 85, 55);
  } else {
    fill(250, 215, 190);
  }
}

function strokeOfEmoji1() {
  if (mouseX > windowWidth / 4 - 150 && mouseX < windowWidth / 4 + 165 && mouseY > windowHeight / 2 - 117.5 && mouseY < windowHeight / 2 + 250) {
    stroke(60, 35, 25);
  } else {
    stroke(230, 193, 166);
  }
}

```

For the second emoji I used only `ellipse()` and `arc()` for the whole emoji. The halo was created with no fill and a heavy stroke:

```
noFill();
stroke(37, 150, 190);
strokeWeight(20);
ellipse(windowWidth / 4 * 3, windowHeight / 2 - 175, 400, 75)
```

Furthermore, the eyes were created with a brown ellipse with a base-color ellipse covering the bottom half, creating the crescent that we see here:

```
fill(150, 100, 50);
stroke(103, 57, 27);
ellipse(windowWidth / 4 * 3 - 75, windowHeight / 2 - 35, 50, 80);
ellipse(windowWidth / 4 * 3 + 75, windowHeight / 2 - 35, 50, 80);

fill(255, 220, 0);
noStroke();
ellipse(windowWidth / 4 * 3 - 75, windowHeight / 2, 50, 80);
ellipse(windowWidth / 4 * 3 + 75, windowHeight / 2, 50, 80);

```

For both of the emojis i used a lot of `push()` and `pop()` to limit the changes in the color of the `stroke()` and the `strokeWeight()` to the individual elements. I am sure it could have been done otherwise as well, but it was the easiest way for me to ensure that the changes I was making was to only a few select elements, which were to have the same characteristics - it made it easier to modify, read and debug.

The .js file contains comments to almost every line of code, explaining what the lines of code do, so for more explanation I refer to the links at the top of the README file`
<br>
<br>

**How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on?**

I chose to create these two emojis based on some research and the assigned video on the basis that some emojis have different meanings and connotations both based on the color/apperance of the emoji but also depending on what culture you live and grow up in.

*1* The first emoji is a raised fist which initially is a light skin color, but when you hover over it the skin color turns darker. This emoji is usually used in solidarity of oppressed and marginalized groups, but can also simply be used as a fist bump or in the context of encouraging others.

The reason I decided to make it interactive comes from the connotations of the Black Lives Matter movement when a black/darker-skinned raised-fist-emoji is displayed. This is a topic that was very debated just a few years ago (and still is), so it more specifically relates to current societal, racial and cultural events.

It is not meant to start a white vs. black discourse, but simply to highlight the different connotations that follows some emojis having light or dark skin displayed.

*2* The second emoji is an angel, which I discovered thorugh my research to have different meanings depending on which culture it is interpreted by. In Western cultures it is displayed as a symbol of innocence, while in China it is a symbol of death. This is not something I knew of beforehand, so I thought it would be an interesting emoji to create, given that is not at all the connotation that we in the Western culture would have seeing it.
<br>
<br>

### References
- [arc(), p5.js Reference](https://p5js.org/reference/#/p5/arc)
- [Drawing in p5.js, BMCC Makerspace](https://openlab.bmcc.cuny.edu/makerspace/drawing-in-p5-js/)
- [Raised Fist Emoji, Dictionary.com](https://www.dictionary.com/e/emoji/raised-fist-emoji/)
- [rect(), p5.js Reference](https://p5js.org/reference/#/p5/rect)
- [The language of emoji: Universal or cross-cultural? by pakt agency, Medium.com](https://medium.com/paktagency/the-language-of-emoji-universal-or-cross-cultural-795214f978ec)