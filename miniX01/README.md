# Æstetisk programmering
### MiniX01
[Link to miniX01 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX01?ref_type=heads)

![](miniX01.gif)

Run the code [here!](https://amalieka.gitlab.io/aestetiskprogrammering/miniX01/minix01.html)

View the code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX01/minix01sketch.js?ref_type=heads) :)
<br>
<br>

**What have you produced?**

My first p5.js code renders a circle bouncing off the edges of the canvas, changing the background color on impact.

I have not used as many different references from the p5js.org website at I could have, but instead focused on practicing defining and using self-made variables, `if` statements for events and the `random()` function in combination with some basic p5.js references.

Using fundamentals of Javascript, I created two `if` statements instead of `if-else` statements. In this way, the change in `backgroundColor` only happens if the `if` statement is true - when the circle collides with the edges of the canvas, and no change occurs when it is false, resulting in this:
```
 if (valueX < 50 || valueX > width - 50) {
    speedX *= -1;
    backgroundColor = color(random(255), random(255), random(255))
  }

  if (valueY < 50 || valueY > height - 50) {
    speedY *= -1;
    backgroundColor = color(random(255), random(255), random(255))
  }
```
I defined all variables before `setup()`, ensuring they can be used throughout the entire code as global variables:
```
let valueX = 700;
let valueY = 100;
let speedX = 10;
let speedY = 5;
let backgroundColor;
```
The .js file contains comments to almost every line of code, explaining what the lines of code do, so for more explanation I refer to the links at the top of the README file.
<br>
<br>

**How would you describe your first independent coding experience (in relation to thinking, reading, copying, modifying, writing code, and so on)?**

I really enjoyed it. I tried several different ideas, e.g. a flower where the petals disappear when you click on them, but decided to go with the idea I have now created after watching videos with similar projects and researching ideas.

I enjoy the independence and creativity this miniX allowed us to explore with no real requirements or limits, so I could try out a bunch of different things and lower or increase my expectations depending on what worked and didn't work.

I spent quite a bit of time researching online and a significant amount of that time was spent attempting to formulate the correct questions so I actually got the results I wanted and needed. ChatGPT was also valuable in terms of getting the result I wanted, and I was able to gather plenty of inspiration from the methods it used as well.

Using the codes I found in my research also supplied me with other ways of thinking about the code I was writing, changing my perspective on my project along the way while also allowing me to explore different codes than what I would otherwise have written myself and playing with the output of different lines of code.
<br>
<br>

### References
- [Bouncing Ball by icm, p5.js Web Editor](https://editor.p5js.org/icm/sketches/BJKWv5Tn)
- [circle(), p5.js Reference](https://p5js.org/reference/#/p5/circle)
- [height, p5.js Reference](https://p5js.org/reference/#/p5/height)
- [noStroke(), p5.js Reference](https://p5js.org/reference/#/p5/noStroke)
- [The Bouncing Ball by The Coding Train, Youtube video](https://www.youtube.com/watch?v=LO3Awjn_gyU&list=PLRqwX-V7Uu6Zy51Q-x9tMWIv9cueOFTFA&index=15)
- [width, p5.js Reference](https://p5js.org/reference/#/p5/width)
