function setup() {
  createCanvas(windowWidth, windowHeight);
  background(255);
  rectMode(CENTER);
}

function draw() {
  emoji1();
  emoji2();
}

function emoji1() {
  noStroke();
  colorOfEmoji1();

  //wrist
  rect(windowWidth / 4, windowHeight / 2 + 150, 200, 200, 20);

  //palm
  rect(windowWidth / 4, windowHeight / 2 + 50, 300, 300, 100);

  //thumb outer part
  ellipse(windowWidth / 4 + 80, windowHeight / 2 + 60, 170, 240);

  //fingers
  push();
  strokeOfEmoji1();
  strokeWeight(5);
  //finger1
  rect(windowWidth / 4 - 125, windowHeight / 2 - 65, 75, 150, 20);
  //finger2
  rect(windowWidth / 4 - 50, windowHeight / 2 - 80, 75, 180, 20);
  //finger3
  rect(windowWidth / 4 + 25, windowHeight / 2 - 80, 75, 180, 20);
  //finger4
  rect(windowWidth / 4 + 100, windowHeight / 2 - 72.5, 75, 165, 20);
  //finger5 / thumb
  rect(windowWidth / 4 + 60, windowHeight / 2 - 15, 180, 75, 20, 20, 0, 20)
  pop();

  //removing stroke on left bottom side of thumb
  rect(windowWidth / 4 + 107.5, windowHeight / 2 + 17.5, 100, 70, 0, 50, 0, 0)

  //creating crease in palm-thumb region
  push();
  strokeOfEmoji1();
  strokeWeight(5);
  rect(windowWidth / 4 + 60, windowHeight / 2 + 47.5, 60, 50, 0, 20, 0, 0);

  //removing right side of rect for the crease - shift x and y 5 px (stroke weight) down and left
  noStroke();
  rect(windowWidth / 4 + 57.5, windowHeight / 2 + 50, 60, 50, 0, 20, 0, 0);
  pop();
}

function colorOfEmoji1() {
  if (mouseX > windowWidth / 4 - 150 && mouseX < windowWidth / 4 + 165 && mouseY > windowHeight / 2 - 117.5 && mouseY < windowHeight / 2 + 250) {
    fill(130, 85, 55);
  } else {
    fill(250, 215, 190);
  }
}

function strokeOfEmoji1() {
  if (mouseX > windowWidth / 4 - 150 && mouseX < windowWidth / 4 + 165 && mouseY > windowHeight / 2 - 117.5 && mouseY < windowHeight / 2 + 250) {
    stroke(60, 35, 25);
  } else {
    stroke(230, 193, 166);
  }
}

function emoji2() {
  //face, emoji 2
  fill(255, 220, 0);
  stroke(0);
  ellipse(windowWidth / 4 * 3, windowHeight / 2, 400)

  //halo, stroke and fill settings should only apply to this element, hence push() and pop()
  push();
  noFill();
  stroke(37, 150, 190);
  strokeWeight(20);
  ellipse(windowWidth / 4 * 3, windowHeight / 2 - 175, 400, 75)
  pop();

  //eyes, push() and pop() again
  //brown ellipses forming the eyes
  push();
  fill(150, 100, 50);
  stroke(103, 57, 27);
  ellipse(windowWidth / 4 * 3 - 75, windowHeight / 2 - 35, 50, 80);
  ellipse(windowWidth / 4 * 3 + 75, windowHeight / 2 - 35, 50, 80);

  //yellow ellipses to remove interior of brown ellipses
  fill(255, 220, 0);
  noStroke();
  ellipse(windowWidth / 4 * 3 - 75, windowHeight / 2, 50, 80);
  ellipse(windowWidth / 4 * 3 + 75, windowHeight / 2, 50, 80);
  pop();

  //mouth
  push();
  noFill();
  stroke(150, 100, 50);
  strokeWeight(5);
  arc(windowWidth / 4 * 3, windowHeight / 2 + 50, 200, 100, 0.2, PI - 0.2);
  pop();
}