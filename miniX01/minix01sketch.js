//Define variables for X and Y values, horizontal (X) and vertical (Y) speed
//as well as the backgroundColor variable used
let valueX = 700;
let valueY = 100;
let speedX = 10;
let speedY = 5;
let backgroundColor;

function setup() {
  createCanvas(1400, 750);
  backgroundColor = color('pink'); //starting background color
}

function draw() {
  //in order for the circle not to "draw" on the page/generate the background color 60 times per second
  background(backgroundColor);

  //creating the circle starting at (700,100 with a diameter of 100)
  noStroke();
  circle(valueX, valueY, 100);

  //the same as "valueX = valueX + speedX" or "valueX = valueX + 10" and the same for Y.
  valueX += speedX;
  valueY += speedY;

  //checking for collisions at the sides/width-wise
  if (valueX < 50 || valueX > width - 50) {
    // the same as "speedX = -1 * speedX", so it continues back off the side of the canvas
    // rather than disappearing out of the canvas
    speedX *= -1;
    //generates a random RGB background color
    backgroundColor = color(random(255), random(255), random(255))
  }

  //checking for collisions at the top and botton/height-wise
  if (valueY < 50 || valueY > height - 50) {
    speedY *= -1;
    backgroundColor = color(random(255), random(255), random(255))
  }
}