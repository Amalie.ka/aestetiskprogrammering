let x = 0;
let y = 0;
let spacing = 50; //all math in the program is defined by spacing, so changing spacing will still make the program work

function setup() {
    createCanvas(1400, 700);
    background(50);

    //creating the horizontal and vertical lines - I believe the horizontal (heigh-wise) are created first
    for (let x = 0; x < width; x += spacing) {
        for (let y = 0; y < height; y += spacing) {
            stroke(75);
            line(x, 0, x, height);
            line(0, y, width, y);
        }
    }
}

function draw() {
    //creating random-colored squares within the boundaries of RGB (50,200)
    fill(random(50, 200), random(50, 200), random(50, 200));
    noStroke();
    rect(x, y, spacing);
    //moves the next square 50 pixels to the right
    x += spacing;
    
    //if the x-coordinate equals or exeeds the width of the canvas, it moves 50 pixels down and starts a new row on the left
    if (x >= width) { //i started by using ==, as the size of the canvas fits with the spacing 1400%50=0, 700%50=0, but this way it works no matter the size of the canvas
        x = 0;
        y += spacing;
    }

    //if the y-coordinate equals or exeeds the height of the canvas, it starts from (0,0)/top left again with new random colors
    if (y >= height) {
        y = 0;
    }
}