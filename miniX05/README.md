# Æstetisk programmering
### MiniX05
[Link to MiniX05 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX05?ref_type=heads)

![](minix05.png)

Run the code [here!](https://Amalieka.gitlab.io/aestetiskprogrammering/miniX05/minix05.html)

View the code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX05/minix05sketch.js) :)
<br>
<br>

**The rules**

1. The colors in the squares are all randomly generated within a range
2. When the program has generated colored squares till the end of the grid (width, height)/bottom right, it starts over at top left again
<br>
<br>

**The program**

The program is super simple with initial grid lines created through nested `for`-loops:

```
for (let x = 0; x < width; x += spacing) {
    for (let y = 0; y < height; y += spacing) {
        stroke(75);
        line(x, 0, x, height);
        line(0, y, width, y);
    }
}
```

...as well as overlaying squares of random color, generated with a `random()` function:

```
fill(random(50, 200), random(50, 200), random(50, 200));
noStroke();
rect(x, y, spacing);
x += spacing;
```

When the x-coordinate of the generated squares equals or exceeds the width of the canvas, it moves on to the row below with an `if`-statement:

```
if (x >= width) {
    x = 0;
    y += spacing;
}
```

A similar thing happens when the y-coordinate equals or exceeds the height (when it reaches the bottom of the canvas), as it starts over from the top, again using an `if`-statement:

```
if (y >= height) {
    y = 0;
}
```

The rules and `if`-statements along with the continuous `draw()` function collectively create a process in which the program theoretically over time will continue indefinitely, as it simply will loop and start from the top again and again and continuously generate random colors for the squares. 

The range for red, green and blue of (50,200) has been chosen purposely for aesthetic reasons, as to avoid the program producing colors close to white, black or very red, blue and green colors for the squares. This limits the colors to a medium of all colors, making it more aesthetically pleasing to look at, in my personal opinion (subjectively).

The grid lines are created in the `setup()` function, hence are created along with the background, and really only have an aesthetic function in the program, as they are covered up by the colored squares over time.

The .js file contains comments to almost every line of code, explaining what the lines of code do, so for more explanation I refer to the links at the top of the README file.
<br>
<br>

**The assinged reading**

Working with the chapter on *Randomness* from the book 10PRINT as well as chapter 5: *Auto-generator* from Aesthetic Programming, we can briefly look at the created program in regards indeterminacy and how it relates to auto-generation.

In regards to the 10PRINT text, the type of indeterminacy this program uses is formal indeterminacy, also known as chance, as the color generated through the `random()` function, can most likely be modeled through statistical models. However, it should be noted that they values are not truly random, as they are modeled through a deterministic algorithm, and are thus pseudorandom. 

In terms of the "auto-generator", this program is not very advanced as does nothing extraordinary or revolutionary, but does use the `random()` function to auto-generate the colors. I have controlled the range of colors which the squares can inhabit, otherwise the program and its algorithms has full autonomy of the resulting colors.

### References
- [10PRINT, "Randomness", Montfort et al.](https://10print.org/10_PRINT_121114.pdf)
- [Aesthetic Programming, chapter 5: "Auto-generator", Soon & Cox](https://aesthetic-programming.net/pages/5-auto-generator.html)
- [Create a grid of 10 columns and 5 rows
by Tiri, p5.js Editor](https://editor.p5js.org/Tiri/sketches/Sk_-uQu6)
- [JavaScript random, Hyperskill University](https://hyperskill.org/university/javascript/javascript-random#:~:text=In%20JavaScript%2C%20the%20Math.,generated%20through%20a%20deterministic%20algorithm.)