//DEFINING VARIABLES
let canvas;
let waves;

//SWIMMER
let swimmer;
let swimmerX;
let swimmerY;

//LIVES
let score = 0;
let lives = 3;

//TRASH VARIABLES
let trash = [];
let minimumTrash = 4;

//IMAGE VARIABLES
let bag;
let bottle;
let coffeecup;
let trashImages = [];


function preload() {
  waves = loadImage('images/wave.png')
  swimmer = loadImage('images/swimmer.png')
  font = loadFont('fonts/badaboom.TTF')

  //LOADING IN THE IMAGES OF THE TRASH
  bag = loadImage('images/bag.png');
  bottle = loadImage('images/bottle.png');
  coffeecup = loadImage('images/coffeecup.png');
}

function setup() {
  //CREATING THE CANVAS
  canvas = createCanvas(500, windowHeight); //assigning the creation of the canvas to a variable, so it can be centered on the window
  canvas.position(windowWidth / 2 - 250, 0); //centering the canvas: the top left of the canvas is at half the window width minus half the width of the canvas

  imageMode(CENTER);

  //INITIAL X AN D YFOR THE SWIMMER
  swimmerY = height - 100;
  swimmerX = 250;

  //ADDING THE IMAGES TO THE trashImages[] ARRAY
  trashImages.push(bag);
  trashImages.push(bottle);
  trashImages.push(coffeecup);
}

function draw() {
  background(128, 223, 255);

  //ADDING THE SWIMMER
  image(swimmer, swimmerX, swimmerY, 100, 150);

  //ADDING THE WAVES
  push();
  tint(255, 50);
  image(waves, 250, height / 4, 300, 100);
  image(waves, 330, height / 4 * 2, 300, 100);
  image(waves, 175, height / 4 * 3, 300, 100);
  pop();

  //CREATING THE HEADER/SCORE TEXT
  createText();

  //CHECK AMOUNT OF TRASH AND PRINT TRASH
  checkTrashAmount();
  showTrash();

  //CHECK COLLISION AND GAME OVER
  checkCollision();
  checkGameOver();
}

function createText() {
  //CREATING HEADER BOX
  push();
  noStroke();
  fill(0, 153, 204, 100);
  rect(0, 0, width, 70)
  pop();

  //CREATING THE TITLE
  push();
  noStroke();
  fill(255);
  textFont(font)
  textSize(35);
  text("ECO EXPLORER: TRASHY TIDES", 20, 47)
  pop();

  //CREATING THE LIFE-TRACKER
  push();
  noStroke();
  fill(0, 94, 128);
  textSize(10);
  text("LIVES REMAINING:", 390, 20)
  textSize(25);
  fill(255);
  text(lives, 425, 50)
  pop();

  //CREATE SCORING BOTTOM-LEFT
  push();
  noStroke();
  fill(255);
  textSize(20);
  text(score, 20, height - 25)
  pop();
}

function checkTrashAmount() {
  if (trash.length < minimumTrash) {
    trash.push(new Trash());
  }

  //INCREASING DIFFICULTY AT HIGHER SCORES
  if(score > 20){
    minimumTrash = 7;
  }
  if(score > 50) {
    minimumTrash = 12;
  }
}

function showTrash() {
  for (let i = 0; i < trash.length; i++) {
    trash[i].move();
    trash[i].show();
  }
}

//CREATING MOVEMENT OF SWIMMER
function keyPressed() {
  //LEFT ARROW
  if (keyCode === 37) {
    swimmerX -= 50;
  }
  //RIGHT ARROW
  if (keyCode === 39) {
    swimmerX += 50;
  }

  //CREATING BOUNDARIES FOR MOVEMENT
  if (swimmerX < 50) { //left boundary
    swimmerX = 50;
  }
  if (swimmerX > 450) { //right boundary
    swimmerX = 450
  }
}

function checkCollision() {
  for (let i = 0; i < trash.length; i++) {
    //CALCULATING DISTANCE BETWEEN SWIMMER AND TRASH
    let distance = dist(swimmerX, swimmerY, trash[i].xPosition, trash[i].yPosition);

    //IF THE TRASH EXCEEDS THE SCREEN, THE SCORE INCREASES
    if (trash[i].yPosition > height + 50) {
      score++;
      console.log(score);
      trash.splice(i, 1);
    }

    //IF THE DISTANCE BETWEEN THE TRASH AND THE SWIMMER IS LESS THAN HALF THE WIDTH OF THE SWIMMER, YOU LOSE A LIFE
    if (distance < 50) {
      lives--;
      console.log(lives);
      trash.splice(i, 1);
    }
  }
}

function checkGameOver() {
  if (lives <= 0) {
    push();
    fill(255, 0, 0);
    textSize(70);
    textFont(font);
    text("GAME OVER", 120, 300);
    pop();
    noLoop();
  }
}