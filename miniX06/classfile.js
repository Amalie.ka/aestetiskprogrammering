class Trash {
    //ASSIGNING ATTRIBUTES TO THE TRASH
    constructor() {
        this.speed = 5;
        this.xPosition = floor(random(1,10)) *50;
        this.yPosition = random(-5,-500);
        this.size = 75;
        this.image = random(trashImages);
    }

    //CREATING MOTION
    move() {
        this.yPosition += this.speed; //moving down by 5 pixels
    }

    //CREATING THE TRASH AS IMAGES
    show() {
        image(this.image,this.xPosition,this.yPosition,this.size, this.size);
    }
}