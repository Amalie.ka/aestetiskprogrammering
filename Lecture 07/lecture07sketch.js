function setup() {
  createCanvas(1000, 750);
  background(125, 220, 255);
}

function draw() {
  fill(255, 200, 150);
  arc(200, 50, 1300, 1300, 0.5, 1.5,PIE)
  fill(250, 235, 90);
  arc(200, 50, 1150, 1150, 0.5, 1.5,PIE)

  fill(175, 45, 55);
  ellipse(500, 375, 100, 100);
  ellipse(350, 500, 100, 100);
  ellipse(360, 300, 100, 100);
  ellipse(350, 500, 100, 100);
  arc(210, 200, 100, 100, 4.65, 1.5, CHORD);
  arc(245, 400, 100, 100, -2.05, 1.92,CHORD);
  arc(525,210,100,100,0.81,3.33,CHORD);
}