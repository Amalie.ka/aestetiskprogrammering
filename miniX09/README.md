# Æstetisk programmering
### MiniX09 - minix04 flowchart
[Link to MiniX09 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX09)

![](minix04_flowchart.png)

Run the minix04 code [here!](https://Amalieka.gitlab.io/aestetiskprogrammering/miniX04/minix04.html)

View the minix04 code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX04/minix04sketch.js?ref_type=heads) :)
<br>
<br>

**What are the difficulties in trying to keep things simple at the communications level whilst maintaining complexity at the algorithmic procedural level?**

Is can be difficult to create a flowchart explaining a process/flow of a program as you need to decide which elements, decisions and processes that need to be included. Some may not seem important, but may actually be when explaining the processes executed within a running program. It can also be difficult to figure out where the elements in the flowchart need to be linked and especially when you need to create a new step in the process or if the process only can be repeated. This is especially apparent in our flowchart on the idea 'Blackout', where it was necessary to determine how many times the eating/drinking processes need to be entered into the flowchart and when it can be repeated through the existing elements in the flowchart.
<br>
<br>

**What are the technical challenges facing the two ideas and how are you going to address these?**

*(Not so) safe data:*

This idea still contains a lot of unknowns. This idea seems to be one of those ideas where I would personally get most of my ideas in the process of coding it, and it is therefore harder to create a full flowchart ahead of the coding. However, the basic elements are established in the flowchart, as in that something is being initialized in a setup function, and most importantly that a process starts once the user clicks a button.

So the primary challenge facing this idea is the actual development of the ideas (the contents, design, and execution) during the coding or at the start of the design-process.

<br>

*Blackout:*

The flowchart for the Blackout idea can be difficult to execute as there are a lot of elements involved. The prospect of the BAC having to increase and decrease involves a lot of different choices that all have to be visualized which is why the flowchart might look complicated at first sight. However, both the intake of liquid and food work in the same way as the intake of water can decrease your BAC just like eating some food would decrease it. 

The timer which works in the background is also an extra element, however, its main job is to help decrease the BAC as time passes, the same way it would in real life, and simply runs in the background without adding any extra technical difficulty to the code.

The code would end up using a lot of the same functions to create these elements which means that the chart looks a lot more complicated than it would be in practice.
<br>
<br>

**In which ways are the individual and the group flowcharts you produced useful?**

- The individual flowcharts are useful for personal understanding, a great training tool, documentation and clarity.
- The group flowcharts are useful for communication, discussion and collaborative planning.

The individual flowcharts were helpful to see how to make them from different perspectives, whether it's the user side or the computer side. It was also helpful to see how much we needed to include, which parts were relevant and which were superfluous, as we had created them with different levels of detail. It's a smart method to use if you need a starting point in your brainstorm.

The group flowcharts are great for discussing ideas about potential projects and for getting an idea about how difficult it is to produce a piece of code. I feel like a code can easily look much more circumstantial when visualized in a flowchart, which may also help dial down the expectations of what we wish to do - often your ideas and ambitions are a tad higher that what you in actuality are able to produce. So in this regard, they are good for organizing your collective thought and ideas and setting the expectations for what you are able to do.
<br>
<br>

### References
- [Aesthetic programming, Chapter 9: Algorithmic procedures (Soon & Cox)](https://aesthetic-programming.net/pages/9-algorithmic-procedures.html)