# Æstetisk programmering
### MiniX04
[Link to MiniX04 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX04?ref_type=heads)

![](minix04.gif)

Run the code [here!](https://Amalieka.gitlab.io/aestetiskprogrammering/miniX04/minix04.html)

View the code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX04/minix04sketch.js?ref_type=heads) :)
<br>
<br>

#### *ADVERTISEMENTS SIMPLIFIED*
##### INSPIRATION: [TRANSMEDIALE 2015 - CALL FOR WORKS](https://archive.transmediale.de/content/call-for-works-2015)

This project is created on the topic of advertisements, cookies and data capture. It is created with the intention of drawing attention to how our online searches and browsing history impact the advertisements that we are shown by companies such as Google and Meta.

This code uses DOM elements such as radio buttons and “regular” buttons to simulate a search of a certain item online. The `event` of the Search button being pressed starts a sequence of `setTimeout` functions that display a chain of data capture, sale, and utilization resulting in an advertisement of the selected items being displayed on the screen.

This is meant to simulate that our online searches are recorded, sold and used by advertisement companies to target us as users with advertisements on exactly what we may be looking for or contemplating buying. This is a topic that is focused more on in recent times and as more laws and regulations come into place, we as users get more say in to who and for what reason we are selling our data.

It is through this, that the project addresses the theme of “Capture All”. We make many searches daily on platforms and websites such as Google, Bing, Facebook and Instagram, where we do not realize what data is being recorded and exactly what it is being used for – this highlights that it is used for advertisements, amongst other things.
<br>
<br>

### References
- [9.4: JavaScript setTimeout() Function - p5.js Tutorial by The Coding Train, YouTube video](https://www.youtube.com/watch?v=nGfTjA8qNDA)
- [Button and text vertical centering by reona396, Processing Forum](https://forum.processing.org/two/discussion/23250/button-and-text-vertical-centering.html)
- [setTimeout() global function, mnd web docs](https://developer.mozilla.org/en-US/docs/Web/API/setTimeout)