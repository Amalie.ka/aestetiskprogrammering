let pizza;
let tablecloth;
let x = 0; //x-coordinate for the first square / initial x-value
let y = 0; //y-coordinate for the first square / initial y-value
let xCoordinates = [80, 0, 80, 0, 80, 0, 80, 0] //starts at x = 0 (see line 3), so the array only contains 8 elements 
let rows = 9;
let numberOfRedSquares = 8; //max number of red squares in a row
let size = 80; //size of a square, both height and width
let distanceBetweenX = 160; //the red squares skip a white (background) square before it prints a new, therefore size * 2
let distanceBetweenY = 80; //a row appears for every square and doesn't skip a square like distanceBetweenX

function preload() {
  pizza = loadImage('pizza.png'); //loads in the image of the pizza, so it can be used in draw()
}

function setup() {
  createCanvas(1200, 720);
  frameRate(20);

  tablecloth = createGraphics(width, height); //creates one whole graphic for the tablecloth that can be inserted in draw() at the size of the canvas

  for (let i = 0; i < rows; i++) {
    for (let j = 0; j < numberOfRedSquares; j++) { //nested loop to generate x-coordinates only for the limited amount of rows
      tablecloth.noStroke(); //assigning characteristics for the tablecloth-graphic
      tablecloth.fill('firebrick');
      tablecloth.rect(x, y, size); //printing the sqaures/rectangles with size 80x80
      x += distanceBetweenX;
    }
    x = xCoordinates[i]; //x-coordinate of the left-most red square (first in each row) alternates between 0 and 80
    y += distanceBetweenY; //increases Y by 80 for each row completed
  }
}

function draw() {
  background(255);
  //loading in the tablecloth graphic along with the background ensures the background doesn't mess with the graphic
  image(tablecloth, 0, 0);

  //creating the "loading..." text
  push();
  textAlign(CENTER); //allows the text to be positioned from the center of the text
  textSize(50);
  fill(255);
  stroke(0);
  strokeWeight(5);
  text("loading...", width / 2, height / 5);
  pop();

  drawElements();
}

function drawElements() {
  let num = 20; //20 different positions for the pizza, increasing will make it spin slower

  translate(width / 2, height / 5 * 3);  //move things to the center

  //360/num >> degree of each ellipse's movement;
  //frameCount%num >> get the remainder that to know which one among 20 possible positions.
  let cir = 360 / num * (frameCount % num);

  push();
  rotate(radians(cir));
  imageMode(CENTER); //the image spins from the center
  //loads the image of the pizza with location (0,0) from the translation,
  //so the real location of the center of the image is (width / 2 , height / 5 * 3)
  //and the size is 500x500
  image(pizza, 0, 0, 500, 500);
  pop();
}