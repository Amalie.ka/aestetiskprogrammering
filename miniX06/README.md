# Æstetisk programmering
### MiniX06
[Link to MiniX06 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX06?ref_type=heads)

![](minix06.png)

Run the code [here!](https://Amalieka.gitlab.io/aestetiskprogrammering/miniX06/minix06.html)

View the sketch/gameplay code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX06/minix06sketch.js) and the class/object code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX06/classfile.js) :)
<br>
<br>

**The game**

This minigame is an obstacle game where the player uses the left and right arrows to control a swimmer in order to avoid colliding with the plastic when swimming in the ocean. You gain a point for every piece of trash you avoid while you lose a life for every piece you collide with. When you achieve a score above 20 points more trash will start appearing, and when you reach above 50 points even more will appear. When you collide with the trash 3 times, you are GAME OVER.
<br>
<br>

**The classes, objects and atributes**

The program uses one class: `class Trash`, and only one unique object instance coming from said class as well: `trash.push(new Trash())`. The objects have multiple attributes/properties outlined within the `constructor()`:

```
this.speed = 5;
this.xPosition = floor(random(1,10)) *50;
this.yPosition = random(-5,-500);
this.size = 75;
this.image = random(trashImages);
```

...as well as two methods:

```
move() {
    this.yPosition += this.speed;
}

show() {
    image(this.image,this.xPosition,this.yPosition,this.size, this.size);
}
```

The effect of the `move()` method is to move the trash with a speed of 300 pixels per second (5 pixels 60 times a second) and the `show()` method displays the object instances as a random image (from three possibilities outlined in the `setup()` function) with random x and y positions and size of 75x75 pixels.

`this.yPosition = random(-5,-500);` works to space out the trash vertically, so it will not generate 4 pieces of trash at the same initial y location (in a horizontal line), but give it a change of occuring at a randomized "height".

The trash are created in an array (`let trash = [];`) so they can be displayed and animated through the methods:

```
function checkTrashAmount() {
  if (trash.length < minimumTrash) {
    trash.push(new Trash());
  }
}

function showTrash() {
  for (let i = 0; i < trash.length; i++) {
    trash[i].move();
    trash[i].show();
  }
}
```
<br>
<br>

**Notable code snippets**

Other notable code snippets from the sketch file are:

```
trashImages.push(bag);
trashImages.push(bottle);
trashImages.push(coffeecup);
```

This snippet in the `setup()` function pushed the preloaded images of the bag, bottle and coffeecup into the array called `trashImages[]`. This allows for a random image from the array to be chosen to be displayed through `show()` as explained above.

```
if(score > 20){
    minimumTrash = 7;
}
if(score > 50) {
    minimumTrash = 12;
}
```

This short and simple code increases the minimum trash amount displayed to increase once you reach a score of above 20 and 50. This allows the difficulty of the game to increase slightly to ensure a better playing experience.

```
function checkCollision() {
  for (let i = 0; i < trash.length; i++) {
    let distance = dist(swimmerX, swimmerY, trash[i].xPosition, trash[i].yPosition);

    if (trash[i].yPosition > height + 50) {
      score++;
      console.log(score);
      trash.splice(i, 1);
    }


    if (distance < 50) {
      lives--;
      console.log(lives);
      trash.splice(i, 1);
    }
  }
}
```

This checks for any collisions between the swimmer and trash. Via this, it increases the score if the trash exceeds the height of the canvas but most importantly decreases the amnount of lives if a collision occurs. In both cases the piece of trash is spliced (`trash.splice(i,1)`) so new pieces can be generated.

The .js file contains comments to almost every line of code, explaining what the lines of code do, so for more explanation I refer to the links at the top of the README file.
<br>
<br>

**The assigned reading**
The book refers to object-oriented programming and abstraction in the following way: *"Examining the pseudo object reveals how abstraction takes place in computation resulting in “computerized material,” in which we only select properties and behaviors that we think are important to be represented in a program, and ignore others."*

This can also be seen in this program in that the `constructor()` only assigns the objects with a select few behaviors and properties/attributes. I could also have modelled the objects to rotate or "wave" in the other direction, but I have only chosed these few attributes and methods, as those were the ones I believed to be the most important for the game to function adequately, but does not reflect reality very accurately: You will most likely never swim through the ocean attempting to avoid colliding with trash consisting only of bags, bottles and coffeecups positioned perfectly upright. Therefore, OOP utilizes abstraction in a way that is "good enough" for the purpose and situation it is presented in, but not necessarily realistically in relation to real life.
<br>
<br>

### References
- [Aesthetic Programming, Chapter 6: Object Abstraction, Soon & Cox](https://aesthetic-programming.net/pages/6-object-abstraction.html)
- [Centering Canvas by jm8785, p5.js Editor](https://editor.p5js.org/jm8785/sketches/r0DMO5Mqj)
- [loadFont(), p5.js Reference](https://p5js.org/reference/#/p5/loadFont)
- [Transparence images, p5.js Examples](https://p5js.org/examples/image-transparency.html)