function abbreviation(firstName, middleName, lastName) {
    if (firstName == "" || lastName == "") {
        return "Missing Information"
    }

    const firstInitial = firstName[0].toUpperCase()
    const lastInitial = lastName[0].toUpperCase()

    if (middleName != "") {
        return firstInitial + " " + middleName[0].toUpperCase() + ". " + lastInitial
        // return `${firstInitial} ${middleName[0].toUppercase()}. ${lastInitial}`
    }
    return firstInitial + lastInitial
}

function abbreviationF(text) {
    let components = text.split(" ")
    return abbreviation(components[0], components[1], components[2])
}

// function isPalindromeByBill(word) {
//     return word == word.reversed()
// }

function isPalindrome(word) {
    //let's imagine word is "madam"
    //how do we check the first m with the last m?
    // word[0] //This will be the first letter of the word
    // word.length //This is the amount of characters in the word
    // word[word.length - 1] //This will be the last letter of the word
    if (word.length <= 1) {
        return true
    }
    if (word[0] != word[word.length - 1]) {
        return false
    }
    //     > "madam".substring(1,3)
    //     < "ad"
    //     > "madam".substring(1,4)
    //     < "ada"
    //     > "madam".length
    //     < 5
    //     > "madam".substring(1,"madam".length - 1)
    //     < "ada"
    //     > "madam".substring(1,4)
    //     < "ada"
    let theWordInTheMiddleWithoutTheFirstAndTheLastLetter = word.substring(1, word.length - 1)
    return isPalindrome(theWordInTheMiddleWithoutTheFirstAndTheLastLetter)
}

console.log("madam? ", isPalindrome("madam"))
console.log("noon? ", isPalindrome("noon"))
console.log("sos? ", isPalindrome("sos"))
console.log("sins? ", isPalindrome("sins"))
console.log("power? ", isPalindrome("power"))

console.log(abbreviationF("Zulfo Omar Vazquez")) //Z O. V

console.log(abbreviation("Germán", "Ariel", "Leiva"))
console.log(abbreviation("Germán", "", "Leiva"))
console.log(abbreviation("", "Ariel", ""))

console.log(abbreviation("germán", "ariel", "leiva"))

let monthlyRevenues = [8045, 8045, 9381, 7832, 1945, 8392, 9000, 6804, 1501, 4361, 2066, 1585]
// Can you find the revenues that are bigger than 8000?
let resultOfFilter = monthlyRevenues.filter(checkIfTheNumberIsBiggerThan8000)
function checkIfTheNumberIsBiggerThan8000(anElementInTheArray) {
    return anElementInTheArray > 8000
}
// What does this line of code print in the console?
console.log(resultOfFilter)

let resultOfFilterLength = monthlyRevenues.filter(checkIfTheNumberIsBiggerThan8000).length
function checkIfTheNumberIsBiggerThan8000(anElementInTheArray) {
    return anElementInTheArray > 8000
}
// What does this line of code print in the console?
console.log(resultOfFilterLength)

let resultOfMap = monthlyRevenues.map(calculateTheDifferenceTo8000)
function calculateTheDifferenceTo8000(anElementInTheArray) {
    return anElementInTheArray - 8000
}
// What does this line of code print in the console?
console.log(resultOfMap)

let students = [
    {
        studentID: 1,
        age: 79,
        givenNames: ["John", "Federico"],
        lastName: "Kennedy",
        isGraduated: false,
        registeredCourses: ["Cooking 101", "Aesthetic Programming"]
    },
    {
        studentID: 2,
        age: 666,
        givenNames: ["Pepita"],
        lastName: "Golondrina",
        isGraduated: true,
        registeredCourses: ["Cooking 101", "Aesthetic Programming", "Pottery"]
    },
    {
        studentID: 235768798,
        age: 13,
        givenNames: ["Pablo", "Rodriguez"],
        lastName: "Escobar",
        isGraduated: true,
        registeredCourses: ["Cooking 101", "Aesthetic Programming", "Export/Import"]
    },
]

//Create a function that receives a student and “calculate" their firstName, 
//the firstName is the first given name.

function calculateFirstName(student) {
    return student.givenNames[0]
}

// Create a new function that:
// 2A Creates a new array that only contains all the studentIDs

function allTheStudentsIDs() {
    return students.map(transformAStudentIntoAnId)
}

function transformAStudentIntoAnId(aStudent) {
    return aStudent.studentID
}

console.log("Result of point 2A")
console.log(allTheStudentsIDs())

//2.B Creates a new array that only contains all the students that are graduated

function graduatedStudents() {
    return students.filter(checkIfStudentIsGraduated)
}

function checkIfStudentIsGraduated(aStudent) {
    return aStudent.isGraduated
}

console.log("Result of point 2B")
// console.log(graduatedStudents())
//If we want to see the names of the graduates students we can do another map
console.log(graduatedStudents().map(transformTheStudentIntoAFirstName))

function transformTheStudentIntoAFirstName(aStudent) {
    return calculateFirstName(aStudent)
}

// 2.C Finds out how many students are graduated
function howManyStudentsAreGraduated() {
    return graduatedStudents().length
}

console.log("Result of point 2B")
console.log(graduatedStudents().map(transformTheStudentIntoAFirstName))

// We will do this next class
// 2.D Finds out how many unique courses these students are registered to
// 2.E Removes the student with the studentID: 42
// 2.F Calculates the average age of these students