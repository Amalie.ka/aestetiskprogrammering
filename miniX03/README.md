# Æstetisk programmering
### MiniX03
[Link to MiniX03 repository](https://gitlab.com/Amalieka/aestetiskprogrammering/-/tree/main/miniX03)

![](miniX03image.png)

Run the code [here!](https://Amalieka.gitlab.io/aestetiskprogrammering/miniX03/minix03.html) **> It will take up to 10 seconds (maybe even more) to load, so take a sip of your coffee in the meantime :)**

View the code [here](https://gitlab.com/Amalieka/aestetiskprogrammering/-/blob/main/miniX03/minix03sketch.js?ref_type=heads) :)
<br>
<br>

I have created a rotating pizza under a text saying "loading...". The text is present to strengthen the understanding that it is a throbber and not just a rotating pizza. Furthermore, the background is created to look like a classic red-white checkered tablecloth.

**What do you want to explore?**

In this miniX, I have experimented with using pictures and graphics in combination with the `background()` in `draw()`. This was tricky, as the background being generated at the rate of the framerate otherwise would have hidden any image or graphic on the canvas. However, using those in draw as well allows them to be generated at the same rate as the background:

```
function draw() {
  background(255);
  image(tablecloth, 0, 0);
```

Additionally, I have used a `for-loop` for creating the background graphic of the checkered tablecloth: 

```
for (let i = 0; i < rows; i++) {
    for (let j = 0; j < numberOfRedSquares; j++) {
      tablecloth.noStroke();
      tablecloth.fill('firebrick');
      tablecloth.rect(x, y, size);
      x += distanceBetweenX;
    }
    x = xCoordinates[i];
    y += distanceBetweenY;
}
```

I have used nested loops in order to create rows. The inner `for-loop` increases the x-value of the rectangles by 160 pixels in lines `tablecloth.rect(x,y,size); x += distanceBetweenX;`. The outer for-loop consists of only two lines. `x = xCoordinates[i]` assigns an x-value to the first red square of the next row from an array of differing 0s and 80s: `let xCoordinates = [80, 0, 80, 0, 80, 0, 80, 0];`. This is what creates the checkered patterns rather than vertical lines of red and white. `y += distanceBetweenY;` moves each row down by 80 pixels, 80 pixels being the same as the size of the squares.

The .js file contains comments to almost every line of code, explaining what the lines of code do, so for more explanation I refer to the links at the top of the README file.
<br>
<br>

**What are the time-related syntaxes/functions that you have used in your program, and why have you used them in this way? How is time being constructed in computation (refer to both the reading materials and your coding)?**

I have used the `framerate()` and `frameCount` function and variable, which are directly related to time in this code. `frameRate()` determines with which frequency the `draw()` function is executed and thereby with which speed the throbber rotates. `frameCount` is a variable which tracks the number of frames drawn since the code was executed/drawn. Thus, with a framerate of 20, `frameCount` will have a value of 20 after the first second. This enables the throbber to rotate continuously depending on how many frames have been drawn since the beginning.

Combined with the fact that I have assigned the number 20 to the variable `num`, it takes exactly 1 second for the pizza to complete one rotation, as there is only 20 possible positions for the pizza. Increasing the number in `num` will make it spin slower, as the value 40 will take 2 seconds for a full spin. Therefore, the variable `num` also affects the perceived speed with which the throbber/pizza spins.

This piece of code uses the human time of 1 second to determine the frequency with which the `frameRate()` function operates, as it is drawings per second. However, beside this link to human time, the computer and code operates entirely within its own local time cycle. `frameCount` is based on the framerate, and it works as one singular linear cycle.

Despite the throbber rotating in a circle, appearing as though it works in small seperate cycles (one cycle per 360 degree rotation) to the human eye, the computer does not distinguish these cycles as seperate, as the rotations are based on the variable `frameCount` which, as mentioned above works in a singular continuous, linear cycle.
<br>
<br>

### References
- [frameCount, p5.js Reference](https://p5js.org/reference/#/p5/frameCount)
- [The Techno-Galactic Guide to Software Observation, p. 88-98](https://monoskop.org/log/?p=20190)
- [Pizza image from cleanpng.com](https://www.cleanpng.com/png-pizza-pepperoni-mozzarella-tomato-sauce-fresh-doug-7979787/download-png.html)