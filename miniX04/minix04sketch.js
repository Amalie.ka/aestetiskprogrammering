//Defining all the variables and DOM elements I'll be using so it is ready
let shirtsRadio;
let pantsRadio;
let sneakersRadio;
let bootsRadio;
let perfumesRadio;
let shirtsAd;
let pantsAd;
let sneakersAd;
let bootsAd;
let perfumesAd;
let searchButton;

function preload() {
  //loads in the images that will be used at the end and assigns them to the variable name
  shirtsAd = loadImage('Ad_pictures/shirts_ad.jpg')
  pantsAd = loadImage('Ad_pictures/pants_ad.jpg')
  sneakersAd = loadImage('Ad_pictures/sneakers_ad.png')
  bootsAd = loadImage('Ad_pictures/boots_ad.jpg')
  perfumesAd = loadImage('Ad_pictures/perfumes_ad.jpg')
}

function setup() {
  createCanvas(1400, 750);
  background('mistyrose');
  //applies changes to the rectangles, texts and images from the center of the elements
  rectMode(CENTER);
  textAlign(CENTER);
  imageMode(CENTER);
  //executes the function in line 36 to draw the search box
  createSearchBox();
  //executes the function searchItem when the search button (drawn in createSearchBox()) is pressed
  searchButton.mousePressed(searchItem);
}

function createSearchBox() {
  rect(700, 190, 200, 300, 5);
  //limits textSize to this text only to ensure that I will not accidentally mess anything else up
  push();
  textSize(17);
  text("What are you", 700, 70);
  text("looking for?", 700, 90);
  pop();
  //executes the function in line 51
  createRadioButtons();
  //creates search button
  searchButton = createButton("Search");
  searchButton.position(700 - searchButton.width / 2, 310)
}

function createRadioButtons() {
  //creates all the different radio buttons and assigns e.g. value "Shirts" to shirtsRadio if it is selected
  shirtsRadio = createRadio();
  shirtsRadio.position(625, 100);
  shirtsRadio.option("Shirts");

  pantsRadio = createRadio();
  pantsRadio.position(625, 125);
  pantsRadio.option("Pants");

  sneakersRadio = createRadio();
  sneakersRadio.position(625, 150);
  sneakersRadio.option("Sneakers");

  bootsRadio = createRadio();
  bootsRadio.position(625, 175);
  bootsRadio.option("Boots");

  perfumesRadio = createRadio();
  perfumesRadio.position(625, 200);
  perfumesRadio.option("Perfumes");

  //initial radio button selected
  shirtsRadio.selected("Shirts");
}

function searchItem() {
  //setTimeout functions will execute a function after a set delay - setTimeout(function, delay (in ms)).
  //these functions will be executed with one second between them (besides advertisement() delayed only with 0,5 second
  //further) with the first being executed after 0,5s, then 1,5, 2,5 and 3,5.
  //The delay counts after the event searchButton.mousePressed from line 33 and the last setTimeout function must
  //therefore have the largest delay
  setTimeout(partOne, 500);
  setTimeout(partTwo, 1500);
  setTimeout(partThree, 2500);
  setTimeout(partFour, 3500);
  setTimeout(advertisement, 4000);
}

function partOne() {
  //creates the arrows with an arc and a triangle
  push();
  noFill();
  stroke('steelblue');
  strokeWeight(5);
  //arc
  arc(850, 325, 400, 300, PI + HALF_PI, 0);
  pop();
  push();
  noStroke();
  fill('steelblue');
  //triangle
  triangle(1000, 300, 1100, 300, 1050, 350);
  pop();
  push();
  textSize(20);
  //text
  text("Collecting Data", 1050, 400);
  pop();
}

function partTwo() {
  //same as partOne just rotated a half pi clock-wise
  push();
  noFill();
  stroke('steelblue')
  strokeWeight(5);
  arc(850, 475, 400, 300, 0, HALF_PI);
  pop();
  push();
  noStroke();
  fill('steelblue');
  triangle(825, 625, 875, 575, 875, 675);
  pop();
  push();
  textSize(20);
  push();
  textSize(30);
  stroke('saddlebrown');
  fill('gold');
  text("$$$", 700, 600);
  pop();
  text("IBuyYourData.com", 700, 625);
  text("buys your data", 700, 650);
  pop();
}

function partThree() {
  //Same as partTwo and partOne rotated another half pi
  push();
  noFill();
  stroke('steelblue')
  strokeWeight(5);
  arc(550, 475, 400, 300, HALF_PI, PI);
  pop();
  push();
  noStroke();
  fill('steelblue');
  triangle(300, 500, 400, 500, 350, 450);
  pop();
  push();
  textSize(20);
  text("IMakeAdvertisements.com", 350, 400)
  text("uses your data for ads", 350, 425)
  pop();
}

function partFour() {
  //rotated a half pi more from partThree, but no text at the end of the arrow
  push();
  noFill();
  stroke('steelblue')
  strokeWeight(5);
  arc(550, 325, 400, 300, PI, PI + HALF_PI);
  pop();
  push();
  noStroke();
  fill('steelblue');
  triangle(575, 175, 525, 125, 525, 225);
  pop();
}

function hideDomElements () {
  //if the radio and search button are not hidden, they will be generated on top of the image, but this hides them
  //this function is used/executed in advertisement().
  shirtsRadio.hide();
  pantsRadio.hide();
  sneakersRadio.hide();
  bootsRadio.hide();
  perfumesRadio.hide();
  searchButton.hide();
}

function advertisement() {
hideDomElements();
//prints the text from refreshToRetry()
refreshToRetry();

//when a radio button is selected, it is assigned the value from createRadioButtons(). These if-statements check if
//the buttons have the value assigned to them (hence, if they are selected) and executes the functions (displays the images)
//if they are selected
  if (shirtsRadio.value() === ("Shirts")) {
    image(shirtsAd, 700, 375, 600, 600);
  }

  if (pantsRadio.value() === ("Pants")) {
    image(pantsAd, 700, 375, 1140, 600);
  }

  if (sneakersRadio.value() === ("Sneakers")) {
    image(sneakersAd, 700, 375, 870, 600);
  }

  if (bootsRadio.value() === ("Boots")) {
    image(bootsAd, 700, 375, 950, 600);
  }

  if (perfumesRadio.value() === ("Perfumes")) {
    image(perfumesAd, 700, 375, 1200, 600);
  }
}

function refreshToRetry() {
  push();
  fill('brown');
  textSize(30);
  text("Refresh to retry", 1200, 50);
  pop();
}